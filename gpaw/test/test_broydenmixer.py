from ase import Atom, Atoms
from gpaw import GPAW
from gpaw.mixer import BroydenMixer
from gpaw.test import equal


def test_broydenmixer(in_tmp_dir):
    a = 2.7
    bulk = Atoms([Atom('Li')], pbc=True, cell=(a, a, a))
    k = 2
    g = 16
    calc = GPAW(gpts=(g, g, g), kpts=(k, k, k), nbands=2,
                mixer=BroydenMixer())
    bulk.calc = calc
    e = bulk.get_potential_energy()
    niter = calc.get_number_of_iterations()
    calc.write('Li.gpw')
    calc2 = GPAW('Li.gpw')

    energy_tolerance = 0.0001
    equal(e, -1.20258, energy_tolerance)
